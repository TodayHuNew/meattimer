//
//  MeatTemperature.swift
//  MeatTimer
//
//  Created by Jonathan Burris on 3/13/15.
//
//

enum MeatTemperature: Int {
   case Rare = 0, MediumRare, Medium, WellDone
    
    var stringValue: String {
        let temperature = ["Rare", "Medium Rare", "Medium", "Well Done"]
        return temperature[self.rawValue]
    }
    
    var timeModifier: Double {
        let modifiers = [0.5, 0.75, 1.0, 1.5]
        return modifiers[self.rawValue]
    }
}
