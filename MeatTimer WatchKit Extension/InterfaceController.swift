//
//  InterfaceController.swift
//  MeatTimer WatchKit Extension
//
//  Created by Jonathan Burris on 3/13/15.
//
//

import WatchKit
import Foundation


class InterfaceController: WKInterfaceController {

    @IBOutlet weak var timer: WKInterfaceTimer!
    @IBOutlet weak var weightLabel: WKInterfaceLabel!
    @IBOutlet weak var cookLabel: WKInterfaceLabel!
    @IBOutlet weak var timerButton: WKInterfaceButton!
    
    var countdownTimer: NSTimer?
    var usingMetric = false
    var timerRunning = false
    var ounces = 16
    var cookTemp = MeatTemperature.Medium
    
    func updateConfiguration() {
        cookLabel.setText(cookTemp.stringValue)
        
        var weight = ounces
        var unit = "oz"
        
        if usingMetric {
            let grams = Double(ounces) * 28.3495
            weight = Int(grams)
            unit = "gm"
        }
        
        weightLabel.setText("Weight: \(weight) \(unit)")
    }
    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        // Configure interface objects here.
        updateConfiguration()
        
    }
    
    func cookTimeForOunces(ounces: Int, cookTemp: MeatTemperature) -> NSTimeInterval {
        let baseTime: NSTimeInterval = 47 * 60
        let baseWeight = 16
        
        let weightModifier: Double = Double(ounces) / Double(baseWeight)
        let tempModifier = cookTemp.timeModifier
        
        return baseTime * weightModifier * tempModifier
    }
    
    @IBAction func onTimerButton() {
        println("onTimerButton")
        
        if timerRunning {
            timer.stop()
            timerButton.setTitle("Start Timer")
            
            countdownTimer = nil
            
        } else {
            let time = cookTimeForOunces(ounces, cookTemp: cookTemp)
            timer.start()
            timerButton.setTitle("Stop Timer")
            
            let countdown: NSTimeInterval = 20
            let date = NSDate(timeIntervalSinceNow: time)
            
            timer.setDate(date)
            timer.start()
            
            countdownTimer = NSTimer.scheduledTimerWithTimeInterval(time, target: self, selector: "timerDone", userInfo: nil, repeats: false)

        }
        
        timerRunning = !timerRunning
        
    }
    
    func timerDone() {
        println("timerDone")
        
        self.presentControllerWithName("timerDone", context: self)
        
    }
    
    @IBAction func onMinusButton() {
        ounces--
        updateConfiguration()
    }

    @IBAction func onPlusButton() {
        ounces++
        updateConfiguration()
    }
    
    @IBAction func onTempChange(value: Float) {
        if let temp = MeatTemperature(rawValue: Int(value)) {
            cookTemp = temp
            updateConfiguration()
        }
    }
    
    @IBAction func onMetricChanged(value: Bool) {
        usingMetric = value
        updateConfiguration()
    }
}
